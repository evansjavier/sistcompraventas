ALTER TABLE `ingreso` ADD `num_comprobante_b` INT UNSIGNED NULL AFTER `num_comprobante`;

ALTER TABLE `venta` ADD `num_comprobante_b` INT UNSIGNED NULL AFTER `num_comprobante`;

INSERT INTO `permiso` (`nombre`, `idpermiso`) VALUES ("Administrar Stock", 8);

ALTER TABLE `persona` ADD `condicion` TINYINT(1) UNSIGNED NULL DEFAULT 1 AFTER `email`;