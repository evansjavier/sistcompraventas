<?php
//Activamos el almacenamiento en el buffer
ob_start();
if (strlen(session_id()) < 1)
    session_start();

if (!isset($_SESSION["nombre"])) {
    echo 'Debe ingresar al sistema correctamente para visualizar el reporte';
} else {
    if ($_SESSION['almacen'] == 1) {

        //Inlcuímos a la clase PDF_MC_Table
        require('PDF_MC_Table.php');

        class PDF extends PDF_MC_Table
        {
            // Cabecera de página
            function Header()
            {

                $this->SetFont('Arial', 'B', 10);
                // Movernos a la derecha
                $this->Cell(136);
                // Título
                $this->Cell(50, 8, "Actualizado: " . date('d-m-Y'), 0, 0, 'R');

                // Salto de línea
                $this->Ln(6);

                $this->Cell(146);
                $this->Cell(40, 12, utf8_decode('Página ') . $this->PageNo(), 0, 0, 'R');
                $this->Ln(15);
            }
        }


        //Instanciamos la clase para generar el documento pdf
        $pdf = new PDF();

        //Agregamos la primera página al documento pdf
        $pdf->AddPage();

        //Seteamos el inicio del margen superior en 25 pixeles 
        $y_axis_initial = 25;

        //Seteamos el tipo de letra y creamos el título de la página. No es un encabezado no se repetirá
        $pdf->SetFont('Arial', 'B', 12);

        $pdf->Cell(40, 6, '', 0, 0, 'C');
        $pdf->Cell(100, 6, 'REPORTE INGRESOS-EGRESOS', 1, 0, 'C');
        $pdf->Ln(10);

        //Creamos las celdas para los títulos de cada columna y le asignamos un fondo gris y el tipo de letra
        $pdf->SetFillColor(232, 232, 232);
        $pdf->SetFont('Arial', 'B', 10);

        $pdf->Cell(17, 6, utf8_decode('Tipo'), 1, 0, 'C', 1);
        $pdf->Cell(39, 6, utf8_decode('Nombre Artículo'), 1, 0, 'C', 1);
        $pdf->Cell(26, 6, utf8_decode('Código'), 1, 0, 'C', 1);
        $pdf->Cell(31, 6, utf8_decode('# Fact / Remito'), 1, 0, 'C', 1);
        $pdf->Cell(35, 6, utf8_decode('Usuario'), 1, 0, 'C', 1);
        $pdf->Cell(22, 6, utf8_decode('Fecha'), 1, 0, 'C', 1);
        // Activar la fila estado
        //$pdf->Cell(20,6,utf8_decode('Estado'),1,0,'C',1);


        $pdf->Ln(10);
        //Comenzamos a crear las filas de los registros según la consulta mysql
        require_once "../modelos/Consultas.php";
        $reporte = new Consultas();

        $rspta = $reporte->reportegeneralingresoventa();

        //Table with rows and columns
        $pdf->SetWidths([17, 39, 26, 31, 35, 22, 20]);

        while ($reg = $rspta->fetch_object()) {
            $tipo = ucfirst($reg->tipo);
            $nombre_articulo = utf8_decode($reg->nombre_articulo);
            $codigo = utf8_decode($reg->codigo);

            $num_comprobante_full = ($reg->tipo == 'compra') ?
                $reg->serie_comprobante . "-" .
                str_pad($reg->num_comprobante, 4, "0", STR_PAD_LEFT) . "-" .
                str_pad($reg->num_comprobante_b, 8, "0", STR_PAD_LEFT)
                :
                str_pad($reg->num_comprobante, 4, "0", STR_PAD_LEFT) . "-" .
                str_pad($reg->num_comprobante_b, 8, "0", STR_PAD_LEFT);

            $nombre_usuario = utf8_decode($reg->nombre_usuario);
            $fecha_hora = date_format(date_create($reg->fecha_hora), "d-m-Y");

            $pdf->SetFont('Arial', '', 10);
            $pdf->Row(
                [
                    $tipo,
                    $nombre_articulo,
                    $codigo,
                    $num_comprobante_full,
                    $nombre_usuario,
                    $fecha_hora,
                    // Activar la fila estado
                    //$reg->estado,
                ]
            );
        }

        //Mostramos el documento pdf
        $pdf->Output();

?>
<?php
    } else {
        echo 'No tiene permiso para visualizar el reporte';
    }
}
ob_end_flush();
?>