<?php 
require_once "global.php";

$conexion = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);

mysqli_query( $conexion, 'SET NAMES "'.DB_ENCODE.'"');

//Si tenemos un posible error en la conexión lo mostramos
if (mysqli_connect_errno())
{
	printf("Falló conexión a la base de datos: %s\n",mysqli_connect_error());
	exit();
}

if (!function_exists('ejecutarConsulta'))
{
	function ejecutarConsulta($sql)
	{
		global $conexion;
		$query = $conexion->query($sql);

		if (!$query) {
			printf("MYSQL Errormessage: %s\n", $conexion->error);

			echo "<br>";
			echo "<pre>";
				var_dump($query);
				var_dump($sql);
			echo "</pre>";

			exit();
		}

		return $query;
	}

	function ejecutarConsultaSimpleFila($sql)
	{
		global $conexion;
		$query = $conexion->query($sql);		

		if (!$query) {
			printf("MYSQL Errormessage: %s\n", $conexion->error);

			echo "<br>";
			echo "<pre>";
				var_dump($query);
			echo "</pre>";

			exit();
		}

		$row = $query->fetch_assoc();
		return $row;
	}

	function ejecutarConsulta_retornarID($sql)
	{
		global $conexion;
		$query = $conexion->query($sql);		

		if (!$query) {
			printf("MYSQL Errormessage: %s\n", $conexion->error);

			echo "<br>";
			echo "<pre>";
				var_dump($query);
			echo "</pre>";

			exit();
		}

		return $conexion->insert_id;			
	}

	function limpiarCadena($str)
	{
		global $conexion;
		$str = mysqli_real_escape_string($conexion,trim($str));
		return htmlspecialchars($str);
	}
}
?>