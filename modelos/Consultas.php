<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Consultas
{
	//Implementamos el constructor
	public function __construct()
	{

	}

	public function comprasfecha($fecha_inicio,$fecha_fin)
	{
		$sql="SELECT DATE(i.fecha_hora) as fecha,u.nombre as usuario, p.nombre 
        as proveedor,i.tipo_comprobante,i.serie_comprobante,i.num_comprobante,i.num_comprobante_b,i.total_compra,
        i.impuesto,i.estado 
        FROM ingreso i INNER JOIN persona p ON i.idproveedor=p.idpersona 
        INNER JOIN usuario u ON i.idusuario=u.idusuario 
        WHERE DATE(i.fecha_hora)>='$fecha_inicio' AND DATE(i.fecha_hora)<='$fecha_fin'";
		return ejecutarConsulta($sql);		
	}

	public function ventasfechacliente($fecha_inicio,$fecha_fin/*,$idcliente*/)
	{
		$sql="SELECT DATE(v.fecha_hora) as fecha,u.nombre as usuario, p.nombre 
        as cliente,v.tipo_comprobante,v.num_comprobante,v.num_comprobante_b,v.total_venta,
        v.impuesto,v.estado 
        FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona 
        INNER JOIN usuario u ON v.idusuario=u.idusuario 
        WHERE DATE(v.fecha_hora)>='$fecha_inicio' AND DATE(v.fecha_hora)<='$fecha_fin'";

        /*AND v.idcliente=idcliente";*/

		return ejecutarConsulta($sql);		
	}

	public function totalcomprahoy()
	{
		$sql="SELECT IFNULL(SUM(total_compra),0) as total_compra FROM ingreso 
		WHERE DATE(fecha_hora)=curdate() AND estado <> 'Anulado'";
		return ejecutarConsulta($sql);
	}

	public function totalventahoy()
	{
		$sql="SELECT IFNULL(SUM(total_venta),0) as total_venta FROM venta 
        WHERE DATE(fecha_hora)=curdate() AND estado <> 'Anulado'";
		return ejecutarConsulta($sql);
	}

	public function comprasultimos_10dias()
	{
		$sql="SELECT CONCAT(DAY(fecha_hora),'-',MONTH(fecha_hora)) as fecha,SUM(total_compra) 
        as total FROM ingreso WHERE estado <> 'Anulado' GROUP by fecha_hora ORDER BY fecha_hora DESC limit 0,10";
		return ejecutarConsulta($sql);
	}

	public function ventasultimos_12meses()
	{
		$sql="SELECT DATE_FORMAT(fecha_hora,'%M') as fecha,SUM(total_venta) as total 
        FROM venta WHERE estado <> 'Anulado' GROUP by MONTH(fecha_hora) ORDER BY fecha_hora DESC limit 0,10";
		return ejecutarConsulta($sql);
	}

	// Reporte de ventas anual ($) agrupada por mes
	public function ventas_periodo_mensual($fecha_inicio,$fecha_fin)
	{
		$sql = "SELECT MONTH(fecha_hora) as fecha,SUM(total_venta) as total
		FROM venta 
		WHERE estado <> 'Anulado' AND DATE(fecha_hora)>='$fecha_inicio' AND DATE(fecha_hora)<='$fecha_fin'
		GROUP by MONTH(fecha_hora) ORDER BY fecha_hora ASC";
		return ejecutarConsulta($sql);
	}

	// Reporte de articulos vendidos (por cantidad)
	public function ventas_articulos_general($fecha_inicio,$fecha_fin)
	{
		$sql = "SELECT a.nombre as nombre, SUM(dv.cantidad) as total_cantidad
		FROM venta v
		INNER JOIN detalle_venta dv
			ON v.idventa = dv.idventa
		INNER JOIN articulo a
			ON a.idarticulo = dv.idarticulo
		WHERE v.estado <> 'Anulado' AND DATE(v.fecha_hora)>='$fecha_inicio' AND DATE(v.fecha_hora)<='$fecha_fin'
		GROUP by (dv.idarticulo) ORDER BY total_cantidad DESC";
		return ejecutarConsulta($sql);
	}

	// Ventas por articulo $ / cantidad
	public function ventas_articulo_cantidad($fecha_inicio,$fecha_fin, $articuloid)
	{
		$sql = "SELECT MONTH(v.fecha_hora) as fecha,SUM(dv.cantidad) as total_cantidad, SUM(v.total_venta) as total_venta
		FROM venta v
		INNER JOIN detalle_venta dv
		ON v.idventa = dv.idventa
		WHERE v.estado <> 'Anulado' AND DATE(v.fecha_hora)>='$fecha_inicio' AND DATE(v.fecha_hora)<='$fecha_fin' AND dv.idarticulo = '$articuloid'
		GROUP by MONTH(v.fecha_hora) ORDER BY v.fecha_hora ASC";
		return ejecutarConsulta($sql);
	}

	public function reportegeneralingresoventa(){

		$sql = "SELECT 	-- LISTADO DE VENTAS
			'venta' as tipo,
			d.iddetalle_venta as iddetalle,
			a.nombre as nombre_articulo,
			a.codigo,
			v.serie_comprobante,
			v.num_comprobante,
			v.num_comprobante_b,
			u.nombre AS nombre_usuario,
			v.fecha_hora,
			v.estado
		FROM detalle_venta d
		INNER JOIN articulo a ON a.idarticulo = d.idarticulo
		INNER JOIN venta v ON d.idventa=v.idventa
		INNER JOIN usuario u ON u.idusuario = v.idusuario

		UNION

		SELECT 	-- LISTADO DE COMPRAS
			'compra' as tipo,
			d.iddetalle_ingreso as iddetalle,
			a.nombre as nombre_articulo,
			a.codigo,
			i.serie_comprobante,
			i.num_comprobante,
			i.num_comprobante_b,
			u.nombre AS nombre_usuario,
			i.fecha_hora,
			i.estado
		FROM detalle_ingreso d
		INNER JOIN articulo a ON a.idarticulo = d.idarticulo
		INNER JOIN ingreso i ON d.idingreso=i.idingreso
		INNER JOIN usuario u ON u.idusuario = i.idusuario

		ORDER BY fecha_hora DESC, serie_comprobante DESC, num_comprobante DESC, num_comprobante_b DESC, iddetalle DESC";

		return ejecutarConsulta($sql);
	}
}

?>