<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";
require_once "../modelos/Articulo.php";

Class Venta
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idcliente,$idusuario,$tipo_comprobante,$serie_comprobante,$num_comprobante,$fecha_hora,$impuesto,$total_venta,$idarticulo,$cantidad,$precio_venta,$descuento)
	{
		$index=0;

		//validar stock de cada articulo
		while ($index < count($idarticulo)){

			$ArticuloModel = new Articulo();
			$articulo = $ArticuloModel->mostrar($idarticulo[$index]);

			if($articulo['stock'] < $cantidad[$index]){
				return "Error. Sólo hay " . $articulo['stock'] . " unidades disponibles de " . $articulo['nombre'];
			}

			$index++;
		}

		$sql="INSERT INTO venta 
			(idcliente,
			idusuario,
			tipo_comprobante,
			serie_comprobante,
			num_comprobante,
			num_comprobante_b,
			fecha_hora,
			impuesto,
			total_venta,
			estado
			)
			VALUES 
			('$idcliente',
			'$idusuario',
			'$tipo_comprobante',
			'$serie_comprobante',
			'$num_comprobante',
			(SELECT COALESCE( (SELECT v.num_comprobante_b FROM (SELECT num_comprobante_b FROM venta) v ORDER BY num_comprobante_b DESC LIMIT 1) , 0) as num_comprobante_b) + 1,
			'$fecha_hora',
			'$impuesto',
			'$total_venta',
			'Aceptado')";

		//return ejecutarConsulta($sql);
		$idventanew=ejecutarConsulta_retornarID($sql);

		$num_elementos=0;
		$sw=true;

		while ($num_elementos < count($idarticulo))
		{
			$sql_detalle = "INSERT INTO detalle_venta(idventa, idarticulo,cantidad,precio_venta,descuento) VALUES ('$idventanew', '$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$precio_venta[$num_elementos]','$descuento[$num_elementos]')";
			ejecutarConsulta($sql_detalle) or $sw = false;
			$num_elementos=$num_elementos + 1;
		}

		return $sw;
	}

	
	//Implementamos un método para anular la venta
	public function anular($idventa)
	{
		$sql="UPDATE venta SET estado='Anulado' WHERE idventa='$idventa'";
		$update_venta = ejecutarConsulta($sql);

		if(!$update_venta)
			return false;

		$detalles = $this->listarDetalle($idventa);
		
		while ($detalle_venta = $detalles->fetch_object())
		{
			// Devolver stock de cada artículo
			$sql_deduce = "UPDATE articulo SET stock = stock + $detalle_venta->cantidad WHERE articulo.idarticulo = $detalle_venta->idarticulo";
			if(!ejecutarConsulta($sql_deduce)){
				return false;
			}			
		}
		return true;
	}


	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idventa)
	{
		$sql="SELECT v.idventa,DATE(v.fecha_hora) as fecha,v.idcliente,p.nombre as cliente,u.idusuario,u.nombre as usuario,v.tipo_comprobante,v.serie_comprobante,v.num_comprobante,v.num_comprobante_b,v.total_venta,v.impuesto,v.estado FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario WHERE v.idventa='$idventa'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function listarDetalle($idventa)
	{
		$sql="SELECT dv.idventa,dv.idarticulo,a.nombre,dv.cantidad,dv.precio_venta,
        dv.descuento,
		ROUND(dv.cantidad * (dv.precio_venta - dv.precio_venta*dv.descuento/100), 2) as subtotal
        FROM detalle_venta dv inner join articulo a on dv.idarticulo=a.idarticulo 
        where dv.idventa='$idventa'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT v.idventa,DATE(v.fecha_hora) as fecha,v.idcliente,p.nombre as 
        cliente,u.idusuario,u.nombre as usuario,v.tipo_comprobante,v.serie_comprobante,
        v.num_comprobante,v.num_comprobante_b,v.total_venta,v.impuesto,v.estado FROM venta v INNER JOIN persona p 
        ON v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario 
        ORDER by v.idventa desc";
		return ejecutarConsulta($sql);		
	}

	public function ventacabecera($idventa){
		$sql="SELECT v.idventa,v.idcliente,p.nombre as cliente,p.direccion,p.tipo_documento,
        p.num_documento,p.email,p.telefono,v.idusuario,u.nombre as usuario,v.tipo_comprobante,
        v.serie_comprobante,v.num_comprobante,v.num_comprobante_b,date(v.fecha_hora) as fecha,v.impuesto,
        v.total_venta FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona 
        INNER JOIN usuario u ON v.idusuario=u.idusuario WHERE v.idventa='$idventa'";
		return ejecutarConsulta($sql);
	}

	public function ventadetalle($idventa){
		$sql="SELECT a.nombre as articulo,a.codigo,d.cantidad,d.precio_venta,d.descuento,
        /*(d.cantidad*d.precio_venta - (d.precio_venta*d.descuento/100))*/
        (d.cantidad*d.precio_venta-d.descuento) as subtotal FROM detalle_venta d 
        INNER JOIN articulo a ON d.idarticulo=a.idarticulo WHERE d.idventa='$idventa'";
		return ejecutarConsulta($sql);
	}

	/**
	 * Fecha de la venta más reciente
	 */
	public function fechaVentaReciente(){
		$sql="SELECT fecha_hora FROM venta ORDER BY fecha_hora DESC";

		return ejecutarConsultaSimpleFila($sql);
	}

	/**
	 * Último número de num_comprobante_b
	 */
	public function ultimoNumComprobanteB(){
		$sql="SELECT COALESCE( (SELECT num_comprobante_b FROM venta ORDER BY num_comprobante_b DESC LIMIT 1), 0) AS num_comprobante_b";
		return ejecutarConsultaSimpleFila($sql);
	}
}
?>