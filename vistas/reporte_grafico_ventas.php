<?php
//Activamos el almacenamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("Location: login.html");
}
else
{
require 'header.php';

if ($_SESSION['consultav']==1)
{
  // Calcular fecha de hace 1 año en formato Y-m-d para setearla como fecha de inicio de los inputs
  $fecha_inicio_date = date_create( date("Y-m-d") );
  date_sub($fecha_inicio_date, date_interval_create_from_date_string('1 year'));
  $fecha_inicio_input =  date_format($fecha_inicio_date, 'Y-m-d');
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">

                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Reporte gráfico de ventas</h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="">

                        <div class="col-xs-12 no-padding">

                          <!-- Main content -->
                          <section class="content">
                            <div class="row">
                              <div class="col-md-6">
                                <!-- AREA CHART -->
                                <div class="box box-primary">
                                  <div class="box-header with-border">
                                    <h3 class="box-title">Reporte de ventas anual</h3>
                                    <div class="box-tools pull-right">
                                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>                                      
                                    </div>
                                    <div class="row no-padding">
                                      <br>
                                      <div class="col-xs-6">
                                          <label>Fecha Inicio</label>
                                          <input type="date" class="form-control" id="reporte_anual_fecha_inicio" value="<?php echo $fecha_inicio_input; ?>">
                                      </div>
                                      <div class="col-xs-6">
                                        <label>Fecha Fin</label>
                                        <input type="date" class="form-control" id="reporte_anual_fecha_fin" value="<?php echo date("Y-m-d"); ?>">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="box-body">
                                    <div class="chart">
                                      <canvas id="areaChart" style="height:250px"></canvas>
                                    </div>
                                  </div><!-- /.box-body -->
                                </div><!-- /.box -->
                                
                                <!-- BAR CHART -->
                                <div class="box box-success">
                                  <div class="box-header with-border">
                                    <h3 class="box-title">Reporte por artículo $/Cantidad</h3>
                                    <div class="box-tools pull-right">
                                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                    <div class="row no-padding">
                                      <br>
                                      <div class="col-xs-12">
                                        <label>Artículo</label>
                                        <select class="form-control" id ="select_articulo">
                                          <option value="">Seleccione</option>
                                        </select>
                                        <br>
                                      </div>
                                      <div class="col-xs-6">
                                          <label>Fecha Inicio</label>
                                          <input type="date" class="form-control" id="reporte_anual_cantidad_inicio" value="<?php echo $fecha_inicio_input; ?>">
                                      </div>
                                      <div class="col-xs-6">
                                        <label>Fecha Fin</label>
                                        <input type="date" class="form-control" id="reporte_anual_cantidad_fin"  value="<?php echo date("Y-m-d"); ?>">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="box-body">
                                    <div class="chart">
                                      <canvas id="barChart" style="height:230px"></canvas>
                                    </div>
                                  </div><!-- /.box-body -->
                                </div><!-- /.box -->

                              </div><!-- /.col (LEFT) -->
                              <div class="col-md-6">
                                <!-- DONUT CHART -->
                                <div class="box box-info">
                                  <div class="box-header with-border">
                                    <h3 class="box-title">Reporte por artículos vendidos</h3>
                                    <div class="box-tools pull-right">
                                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                    <div class="row no-padding">
                                      <br>
                                      <div class="col-xs-6">
                                          <label>Fecha Inicio</label>
                                          <input type="date" class="form-control" id="reporte_general_fecha_inicio" value="<?php echo $fecha_inicio_input; ?>">
                                      </div>
                                      <div class="col-xs-6">
                                        <label>Fecha Fin</label>
                                        <input type="date" class="form-control" id="reporte_general_fecha_fin"  value="<?php echo date("Y-m-d"); ?>">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="box-body">
                                    <div class="chart">
                                    <canvas id="pieChart" style="height:250px"></canvas>
                                    </div>
                                  </div>
                                </div>
                                
                              </div><!-- /.col (RIGHT) -->
                            </div><!-- /.row -->

                          </section><!-- /.content -->


                        </div>



                    </div>
                    
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require 'noacceso.php';
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/reporte_grafico_ventas.js"></script>
<?php 
}
ob_end_flush();
?>


