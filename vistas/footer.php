    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2020 <a href="https://www.linkedin.com/in/juan-ignacio-carabante-453708128/">Juan Carabante</a> /
        <a href="https://www.linkedin.com/in/nicol%C3%A1s-eduardo-gigena-varas-454425184/">Nicolas Gigena</a></strong>. All rights reserved.
    </footer>    
    <!-- jQuery -->
    <!-- <script src="../public/js/jquery-3.1.1.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../public/js/bootstrap.min.js"></script>
    <!-- Chart JS-->
    <script src="../public/plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../public/js/app.min.js"></script>

    <!-- DATATABLES -->
    <script src="../public/datatables/jquery.dataTables.min.js"></script>    
    <!--<script src="../public/datatables/dataTables.buttons.min.js"></script>-->
    <script src="../public/datatables/buttons.html5.min.js"></script>
    <script src="../public/datatables/buttons.colVis.min.js"></script>
    <script src="../public/datatables/jszip.min.js"></script>
    <script src="../public/datatables/pdfmake.min.js"></script>
    <script src="../public/datatables/vfs_fonts.js"></script> 

    <!-- ULTIMAS AGREGADAS PARA EDITAR EXCEL -->
    <script src="../public/datatables/LAST_dataTables.buttons.min.js"></script>
    <script src="../public/datatables/LAST_buttons.html5.min.js"></script>

    <script src="../public/js/bootbox.min.js"></script> 
    <script src="../public/js/bootstrap-select.min.js"></script>

    <script src="./scripts/utils.js"></script>
  </body>
</html>