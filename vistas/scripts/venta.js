var tabla;

//Función que se ejecuta al inicio
function init(){
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	});
	//Cargamos los items al select cliente
	$.post("../ajax/venta.php?op=selectCliente", function(r){
	            $("#idcliente").html(r);
	            $('#idcliente').selectpicker('refresh');
	});

	$.post("../ajax/venta.php?op=selectCliente", function(r){
		$("#clientes_all_respaldo").html(r);
	});
	$.post("../ajax/venta.php?op=selectClienteActivo", function(r){
		$("#clientes_activos_respaldo").html(r);
	});

	$('#mVentas').addClass("treeview active");
    $('#lVentas').addClass("active");
}

//Función limpiar
function limpiar()
{
	$("#idventa").val("");
	$("#idcliente").val("").change();
	$("#cliente").val("");
	$("#serie_comprobante").val("");
	$("#num_comprobante").val("0001");
	$("#num_comprobante_b").val($("#last_comprobante_b").val());	
	$("#impuesto").val("0");

	$("#total_venta").val("");
	$(".filas").remove();
	$("#total").html("0");

	//Obtenemos la fecha actual	
	$('#fecha_hora').val( new Date().toISOString().split("T")[0] );

    //Marcamos el primer tipo_documento
	$("#tipo_comprobante").val($("#tipo_comprobante option:first").val()).change();
	$("#tipo_comprobante").selectpicker('refresh');
}

//Función mostrar formulario
function mostrarform(flag)
{
	if (flag)
	{
		limpiar();
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		//$("#btnGuardar").prop("disabled",false);
        $("#btnagregar").hide();
        $("#btnReporte").hide();
		listarArticulos();

		$("#btnGuardar").hide();
		$("#btnCancelar").show();
		$("#btnAgregarCliente").show();
		$("#btnAgregarArt").show();
		detalles=0;

		cargarClientes(true);
		$("*[data-id=idcliente]").prop("disabled", false);

	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
        $("#btnagregar").show();
        $("#btnReporte").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#tbllistado').dataTable(
        {
            "lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar
            "aProcessing": true,//Activamos el procesamiento del datatables
            "aServerSide": true,//Paginación y filtrado realizados por el servidor
            dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla
            buttons: [{
                extend: 'excelHtml5',
                title: 'REPORTE DE VENTAS',
                messageTop: 'Fecha y hora: '+ new Date(Date.now()).toLocaleString(),
                filename: 'REPORTE DE VENTAS',
                exportOptions: {
                    columns: [ 1,2,3,4,5,6,7 ]
                },  
                },
            ],
		"ajax":
				{
					url: '../ajax/venta.php?op=listar',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"language": {
            "lengthMenu": "Mostrar : _MENU_ registros",
            "buttons": {
            "copyTitle": "Tabla Copiada",
            "copySuccess": {
                    _: '%d líneas copiadas',
                    1: '1 línea copiada'
                }
            }
        },
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}


//Función ListarArticulos
function listarArticulos()
{
	tabla=$('#tblarticulos').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            
		        ],
		"ajax":
				{
					url: '../ajax/venta.php?op=listarArticulosVenta',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}

/**
 * Inserta en el select los proveedores
 */
function cargarClientes(soloActivos = false){

	let dataSelect  = '';
	if(soloActivos){
		dataSelect = $("#clientes_activos_respaldo").html();
	}
	else{
		dataSelect = $("#clientes_all_respaldo").html();
	}

	$("#idcliente").html(dataSelect);
	$('#idcliente').selectpicker('refresh');
}

//Función para guardar o editar
function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	//$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/venta.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          listar();
	    }

	});
	limpiar();
}

function mostrar(idventa)
{
	$.post("../ajax/venta.php?op=mostrar",{idventa : idventa}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		cargarClientes(false);

		$("#idcliente").val(data.idcliente);
		$("#idcliente").selectpicker('refresh');
		$("#tipo_comprobante").val(data.tipo_comprobante);
		$("#tipo_comprobante").selectpicker('refresh');
		$("#serie_comprobante").val(data.serie_comprobante);
		$("#num_comprobante").val(data.num_comprobante);
		$("#num_comprobante_b").val(data.num_comprobante_b);
		$("#fecha_hora").val(data.fecha);
		$("#impuesto").val(data.impuesto);
		$("#idventa").val(data.idventa);

		//Ocultar y mostrar los botones
		$("#btnGuardar").hide();
		$("#btnCancelar").show();
		$("#btnAgregarCliente").hide();
		$("#btnAgregarArt").hide();

		$("*[data-id=idcliente]").prop("disabled", true);

 	});

 	$.post("../ajax/venta.php?op=listarDetalle&id="+idventa,function(r){
	        $("#detalles").html(r);
	});
}

//Función para anular registros
function anular(idventa)
{
	bootbox.confirm("¿Está Seguro de anular la venta?", function(result){
		if(result)
        {
        	$.post("../ajax/venta.php?op=anular", {idventa : idventa}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Declaración de variables necesarias para trabajar con las compras y
//sus detalles
var impuesto=18;
var cont=0;
var detalles=0;
//$("#guardar").hide();
$("#btnGuardar").hide();
$("#tipo_comprobante").change(marcarImpuesto);

function marcarImpuesto()
  {
  	var tipo_comprobante=$("#tipo_comprobante option:selected").text();
  	if (tipo_comprobante=='Factura')
    {
        $("#impuesto").val(impuesto); 
    }
    else
    {
        $("#impuesto").val("0"); 
    }
  }

function agregarDetalle(idarticulo,articulo,precio_venta, stock)
  {
  	var cantidad=1;
    var descuento=0;
	
	if( $(".detalle_venta_idarticulo[value=" + idarticulo + "]").length ){
		alert("Ya fue incluido");
		return false;
	}

	if( stock < 1 ){
		alert("No hay stock disponible");
		return false;
	}

    if (idarticulo!="")
    {
    	var subtotal=cantidad*precio_venta;
    	var fila='<tr class="filas" id="fila'+cont+'">'+
    	'<td><button type="button" class="btn btn-danger" onclick="eliminarDetalle('+cont+')">X</button></td>'+
    	'<td><input type="hidden" class="detalle_venta_idarticulo" name="idarticulo[]" value="'+idarticulo+'">'+articulo+'</td>'+
    	'<td><input required min="1" max="' + stock +'" pattern="^[0-9]+" type="number" name="cantidad[]" id="cantidad[]" value="'+cantidad+'"></td>'+
    	'<td><input required min="1" pattern="^[0-9]+" type="number" name="precio_venta[]" id="precio_venta[]" value="'+precio_venta+'"></td>'+
    	'<td><input required min="0" max="100" pattern="^[0-9]+" type="number" name="descuento[]" value="'+descuento+'"></td>'+
    	'<td><span name="subtotal" id="subtotal'+cont+'">'+subtotal+'</span></td>'+
    	'<td><button type="button" onclick="modificarSubototales()" class="btn btn-info"><i class="fa fa-refresh"></i></button></td>'+
    	'</tr>';
    	cont++;
    	detalles=detalles+1;
    	$('#detalles').append(fila);
    	modificarSubototales();
    }
    else
    {
    	alert("Error al ingresar el detalle, revisar los datos del artículo");
    }
  }

  function modificarSubototales()
  {
  	var cant = document.getElementsByName("cantidad[]");
    var prec = document.getElementsByName("precio_venta[]");
    var desc = document.getElementsByName("descuento[]");
    var sub = document.getElementsByName("subtotal");

    for (var i = 0; i <cant.length; i++) {

		// inputs
    	var inpCantidad=cant[i];
    	var inpPrecio=prec[i];
    	var inpDescuento=desc[i];
		var inpSubtotal=sub[i];

		if(inpDescuento.value < 0){
			inpDescuento.value = -inpDescuento.value;
		}

		if(inpDescuento.value > 100)
			inpDescuento.value = 100;

		var valorDescuento = inpPrecio.value * ( inpDescuento.value / 100 ); // descuento individual

		// subtotal
		let subtotal = inpCantidad.value * (inpPrecio.value - valorDescuento );
		subtotal = parseFloat( subtotal.toFixed(2) );
    	inpSubtotal.value = subtotal;
    	document.getElementsByName("subtotal")[i].innerHTML = subtotal;
    }
    calcularTotales();

  }
  function calcularTotales(){
  	var sub = document.getElementsByName("subtotal");
  	var total = 0.0;

  	for (var i = 0; i <sub.length; i++) {
		total += document.getElementsByName("subtotal")[i].value;
	}
	$("#total").html("$. " + total);
    $("#total_venta").val(total);
    evaluar();
  }

  function evaluar(){
  	if (detalles>0)
    {
      $("#btnGuardar").show();
    }
    else
    {
      $("#btnGuardar").hide(); 
      cont=0;
    }
  }

  function eliminarDetalle(indice){
  	$("#fila" + indice).remove();
  	calcularTotales();
  	detalles=detalles-1;
  	evaluar()
  }

init();