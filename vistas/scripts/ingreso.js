var tabla;

//Función que se ejecuta al inicio
function init(){
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	});
	//Precargamos los items de proveedores
	$.post("../ajax/ingreso.php?op=selectProveedor", function(r){
		$("#proveedores_all_respaldo").html(r);
	});
	$.post("../ajax/ingreso.php?op=selectProveedorActivo", function(r){
		$("#proveedores_activos_respaldo").html(r);
	});
	$('#mCompras').addClass("treeview active");
    $('#lIngresos').addClass("active");	
}

//Función limpiar
function limpiar()
{
	$("#idingreso").val("");
	$("#proveedor").val("");
	$("#num_comprobante").val("0001");
	$("#num_comprobante_b").val("");
	$("#impuesto").val("0");

	$("#total_compra").val("");
	$(".filas").remove();
	$("#total").html("0");
	
	//Seteamos la fecha actual
	$('#fecha_hora').val( new Date().toISOString().split("T")[0] );

	//Marcamos la primer opción de los select
	$("#idproveedor").val("").change();
	$("#tipo_comprobante").val($("#tipo_comprobante option:first").val()).change();
	$("#serie_comprobante").val($("#serie_comprobante option:first").val()).change();

	$("#tipo_comprobante").selectpicker('refresh');
}

//Función mostrar formulario
function mostrarform(flag)
{
	
	if (flag)
	{
		limpiar();
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		//$("#btnGuardar").prop("disabled",false);
        $("#btnagregar").hide();
        $("#btnReporte").hide();
		listarArticulos();

		$("#btnGuardar").hide();
		$("#btnCancelar").show();
		detalles=0;
		$("#btnAgregarArt").show();

		cargarProveedores(true);

		$("*[data-id=idproveedor]").prop("disabled", false);
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
        $("#btnagregar").show();
		$("#btnAgregarProveedor").show();
        $("#btnReporte").show();
	}

}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#tbllistado').dataTable(
        {
            "lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar
            "aProcessing": true,//Activamos el procesamiento del datatables
            "aServerSide": true,//Paginación y filtrado realizados por el servidor
            dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla
            buttons: [{
                extend: 'excelHtml5',
                title: 'REPORTE DE INGRESOS',
                messageTop: 'Fecha y hora: '+ new Date(Date.now()).toLocaleString(),
                filename: 'REPORTE DE INGRESOS',
                exportOptions: {
                    columns: [ 1,2,3,4,5,6,7 ]
                },  
                },
            ],
		"ajax":
				{
					url: '../ajax/ingreso.php?op=listar',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"language": {
            "lengthMenu": "Mostrar : _MENU_ registros",
            "buttons": {
            "copyTitle": "Tabla Copiada",
            "copySuccess": {
                    _: '%d líneas copiadas',
                    1: '1 línea copiada'
                }
            }
        },
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}


//Función ListarArticulos
function listarArticulos()
{
	tabla=$('#tblarticulos').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            
		        ],
		"ajax":
				{
					url: '../ajax/ingreso.php?op=listarArticulos',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}

/**
 * Inserta en el select los proveedores
 */
function cargarProveedores(soloActivos = false){

	let dataSelect  = '';
	if(soloActivos){
		dataSelect = $("#proveedores_activos_respaldo").html();
	}
	else{
		dataSelect = $("#proveedores_all_respaldo").html();
	}

	$("#idproveedor").html(dataSelect);
	$('#idproveedor').selectpicker('refresh');
}

//Función para guardar o editar
function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	//$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/ingreso.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          listar();
	    }

	});
	limpiar();
}

function mostrar(idingreso)
{
	
	$.post("../ajax/ingreso.php?op=mostrar",{idingreso : idingreso}, function(data, status)
	{
		data = JSON.parse(data);		
		data_ingreso = data;
		mostrarform(true);
		cargarProveedores(false);

		$("#idproveedor").val(data.idproveedor);
		$("#idproveedor").selectpicker('refresh');
		$("#tipo_comprobante").val(data.tipo_comprobante).change();
		$("#tipo_comprobante").selectpicker('refresh');
		$("#serie_comprobante").val(data.serie_comprobante).change();
		$("#num_comprobante").val(data.num_comprobante);
		$("#num_comprobante_b").val(data.num_comprobante_b);
		$("#fecha_hora").val(data.fecha);
		$("#impuesto").val(data.impuesto);
		$("#idingreso").val(data.idingreso);

		//Ocultar y mostrar los botones
		$("#btnGuardar").hide(); // show() para activar la edición
		$("#btnCancelar").show();
		$("#btnAgregarArt").hide();
		$("#btnAgregarProveedor").hide();

		$("*[data-id=idproveedor]").prop("disabled", true);

		$.post("../ajax/ingreso.php?op=listarDetalle&id="+idingreso,function(r){
				$("#detalles").html(r);
				$("#total_compra").val(data.total_compra);
		});

 	});
}

//Función para anular registros
function anular(idingreso)
{
	bootbox.confirm("¿Está Seguro de anular el ingreso?", function(result){
		if(result)
        {
        	$.post("../ajax/ingreso.php?op=anular", {idingreso : idingreso}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Declaración de variables necesarias para trabajar con las compras y
//sus detalles
var impuesto=18;
var cont=0;
var detalles=0;
//$("#guardar").hide();
$("#btnGuardar").hide();
//$("#tipo_comprobante").change(marcarImpuesto);

/*function marcarImpuesto()
  {
  	var tipo_comprobante=$("#tipo_comprobante option:selected").text();
  	if (tipo_comprobante=='Factura')
    {
        $("#impuesto").val(impuesto); 
    }
    else
    {
        $("#impuesto").val("0"); 
    }
  }*/

function agregarDetalle(idarticulo,articulo,precio_venta = 1)
  {
  	var cantidad=1;
    var precio_compra=1;    

	if( $(".detalle_idarticulo[value=" + idarticulo + "]").length ){
		alert("Ya fue incluido");
		return false;
	}

    if (idarticulo!="")
    {
    	var subtotal=cantidad*precio_compra;
    	var fila='<tr class="filas" id="fila'+cont+'">'+
    	'<td><button type="button" class="btn btn-danger" onclick="eliminarDetalle('+cont+')">X</button></td>'+
		'<td><input type="hidden" class="detalle_idarticulo" name="idarticulo[]" value="'+idarticulo+'">'+articulo+'</td>'+
    	'<td><input min="1" pattern="^[0-9]+" type="number" name="cantidad[]" id="cantidad[]" value="'+cantidad+'"></td>'+
    	'<td><input min="1" pattern="^[0-9]+" type="number" name="precio_compra[]" id="precio_compra[]" value="'+precio_compra+'"></td>'+
    	'<td><input min="1" pattern="^[0-9]+" type="number" name="precio_venta[]" value="'+precio_venta+'"></td>'+
    	'<td><span name="subtotal" id="subtotal'+cont+'">'+subtotal+'</span></td>'+
    	'<td><button type="button" onclick="modificarSubototales()" class="btn btn-info"><i class="fa fa-refresh"></i></button></td>'+
    	'</tr>';
    	cont++;
    	detalles=detalles+1;
    	$('#detalles').append(fila);
    	modificarSubototales();
    }
    else
    {
    	alert("Error al ingresar el detalle, revisar los datos del artículo");
    }
  }

  function modificarSubototales()
  {
  	var cant = document.getElementsByName("cantidad[]");
    var prec = document.getElementsByName("precio_compra[]");
    var sub = document.getElementsByName("subtotal");

    for (var i = 0; i <cant.length; i++) {
    	var inpC=cant[i];
    	var inpP=prec[i];
    	var inpS=sub[i];

    	inpS.value=inpC.value * inpP.value;
    	document.getElementsByName("subtotal")[i].innerHTML = inpS.value;
    }
    calcularTotales();

  }
  function calcularTotales(){
  	var sub = document.getElementsByName("subtotal");
  	var total = 0.0;

  	for (var i = 0; i <sub.length; i++) {
		total += document.getElementsByName("subtotal")[i].value;
		console.log("sumando", document.getElementsByName("subtotal")[i].value );
	}
	$("#total").html("$. " + total);
	console.log("se setea total_compra", total);
    $("#total_compra").val(total);
    evaluar();
  }

  function evaluar(){

	console.log("detalles", detalles);

  	if (detalles>0)
    {
      $("#btnGuardar").show();
    }
    else
    {
      $("#btnGuardar").hide(); 
      cont=0;
    }
  }

  function eliminarDetalle(indice){
  	$("#fila" + indice).remove();
  	calcularTotales();
  	detalles=detalles-1;
  	evaluar();
  }

init();