// Variables para guardar gráficas
var reporteVentasAnual = null; // grafica de ventas anual (chartjs)
var reporteGeneral = null; // grafica de articulos vendidos,general (chartjs)
var reporteArticuloCantidad = null; // grafica de reporte articulo/cantidad, (chartjs)

//Función que se ejecuta al inicio
function init(){
	
	$('#mReportes').addClass("treeview active");
	$('#lReporteGraficoVentas').addClass("active");
  
  cargarArticulos(); // Cargar opciones al select de articulos
  
  actualizaReporteAnual();          // Cargar data grafico ventas anual
  actualizaReporteAnualCantidad();  // Cargar data grafico ventas + cantidades anual
  actualizaReporteGeneral();        // Cargar data grafico ventas de todos los artículos

  // Actualizar gráfico si las fechas cambian
  $("#reporte_anual_fecha_inicio").change(actualizaReporteAnual);
  $("#reporte_anual_fecha_fin").change(actualizaReporteAnual);
  
  // Actualizar gráfico si las fechas cambian
  $("#reporte_general_fecha_inicio").change(actualizaReporteGeneral);
  $("#reporte_general_fecha_fin").change(actualizaReporteGeneral);
  
  // Actualizar gráfico si las fechas o artículo cambian
  $("#reporte_anual_cantidad_inicio").change(actualizaReporteAnualCantidad);
  $("#reporte_anual_cantidad_fin").change(actualizaReporteAnualCantidad);
  $("#select_articulo").change(actualizaReporteAnualCantidad);

}


//Actualizar data del reporte anual
function actualizaReporteAnual()
{
	  console.log("actualizando reporte anual");

    var fecha_inicio = $("#reporte_anual_fecha_inicio").val();
    var fecha_fin = $("#reporte_anual_fecha_fin").val();

    $.ajax({
      url: '../ajax/consultas.php?op=ventas_periodo_mensual&fecha_inicio=' + fecha_inicio + '&fecha_fin=' + fecha_fin,
        type: "GET",
        dataType: 'json',

    }).done(function(datos){

        var areaChartCanvas = $("#areaChart").get(0).getContext("2d"); // Obtener contexto del canvas
        var areaChart = new Chart(areaChartCanvas);

        // Datos del gráfico de área
        var areaChartData = {
          labels: datos.meses,
          datasets: [
            {
              label: "Ventas por mes",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              pointColor: "#3b8bba",
              pointStrokeColor: "rgba(60,141,188,1)",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(60,141,188,1)",
              data: datos.datos
            }
          ]
        };

        // Opciones del gráfico
        var areaChartOptions = {
          showScale: true,
          scaleShowGridLines: false,
          scaleGridLineColor: "rgba(0,0,0,.05)",
          scaleGridLineWidth: 1,
          scaleShowHorizontalLines: true,
          scaleShowVerticalLines: true,
          bezierCurve: true,
          bezierCurveTension: 0.3,
          pointDot: false,
          pointDotRadius: 4,
          pointDotStrokeWidth: 1,
          pointHitDetectionRadius: 20,
          datasetStroke: true,
          datasetStrokeWidth: 2,
          datasetFill: true,
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          maintainAspectRatio: true,
          responsive: true
        };

        if(reporteVentasAnual){ // destruir gráfico antiguo
          reporteVentasAnual.destroy();
        }

        reporteVentasAnual = areaChart.Line(areaChartData, areaChartOptions);
      
    })
    .fail(function(datos){
      alert("Hubo un error 1");
    });

}


//Actualizar reporte general de articulos vendidos
function actualizaReporteGeneral()
{
	  console.log("actualizando reporte anual general");

    var fecha_inicio = $("#reporte_general_fecha_inicio").val();
    var fecha_fin = $("#reporte_general_fecha_fin").val();

    $.ajax({
      url: '../ajax/consultas.php?op=ventas_articulos_general&fecha_inicio=' + fecha_inicio + '&fecha_fin=' + fecha_fin,
        type: "GET",
        dataType: 'json',

    }).done(function(datos){
      
      // array con colores a usar en grafico "pie" (torta)
      var colores = [
        "#f56954",
        "#00a65a",
        "#f39c12",
        "#00c0ef",
        "#3c8dbc",
        "#d2d6de"
      ];
      
      var pieChartCanvas = $("#pieChart").get(0).getContext("2d"); // Obtener contexto del canvas
      var pieChart = new Chart(pieChartCanvas);

      // Datos del gráfico
      var PieData = [];
      datos.articulos.forEach(function(nombre_articulo, i){

        PieData.push({
            value: datos.datos_cantidades[i],
            color: colores[i%6],      // Calcular el residuo para seleccionar un color del array
            highlight: colores[i%6],  // Calcular el residuo para seleccionar un color del array
            label: nombre_articulo
        });

      });

      // Opciones del gráfico
      var pieOptions = {
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 50,
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true,
        maintainAspectRatio: true,
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
      };
      
      if(reporteGeneral){ // destruir gráfico antiguo
        reporteGeneral.destroy();          
      }

      reporteGeneral = pieChart.Doughnut(PieData, pieOptions);

    })
    .fail(function(datos){
      alert("Hubo un error 2");
    });

}
//Actualizar data del reporte anual con cantidades
function actualizaReporteAnualCantidad()
{
    console.log("actualizando reporte anual por cantidades");

    var fecha_inicio = $("#reporte_anual_cantidad_inicio").val();
    var fecha_fin = $("#reporte_anual_cantidad_fin").val();
    var articuloid = $("#select_articulo").val();

    $.ajax({
      url: '../ajax/consultas.php?op=ventas_articulo_cantidad&articuloid=' + articuloid + '&fecha_inicio=' + fecha_inicio + '&fecha_fin=' + fecha_fin,
        type: "GET",
        dataType: 'json',

    }).done(function(datos){
      
        var barChartCanvas = $("#barChart").get(0).getContext("2d"); // Obtener contexto del canvas
        var barChart = new Chart(barChartCanvas);
        
        // Datos del gráfico de barras
        var barChartData = {
          labels: datos.meses,
          datasets: [
            {
              label: "Cantidad",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: datos.datos_cantidades
            },
            {
              label: "Pesos $",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              pointColor: "#3b8bba",
              pointStrokeColor: "rgba(60,141,188,1)",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(60,141,188,1)",
              data: datos.datos_ventas
            }
          ]
        };

        barChartData.datasets[1].fillColor = "#00a65a";
        barChartData.datasets[1].strokeColor = "#00a65a";
        barChartData.datasets[1].pointColor = "#00a65a";

        // Opciones del gráfico de barras
        var barChartOptions = {
          scaleBeginAtZero: true,
          scaleShowGridLines: true,
          scaleGridLineColor: "rgba(0,0,0,.05)",
          scaleGridLineWidth: 1,
          scaleShowHorizontalLines: true,
          scaleShowVerticalLines: true,
          barShowStroke: true,
          barStrokeWidth: 2,
          barValueSpacing: 5,
          barDatasetSpacing: 1,
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          responsive: true,
          maintainAspectRatio: true
        };

        barChartOptions.datasetFill = false;

        if(reporteArticuloCantidad){ // destruir gráfico antiguo
          reporteArticuloCantidad.destroy();          
        }

        reporteArticuloCantidad = barChart.Bar(barChartData, barChartOptions);

    })
    .fail(function(datos){
      alert("Hubo un error 3");
    });

}

// Obtener listado de articulos y rellenar opciones del select
function cargarArticulos(){
  
  $.ajax({
    url: '../ajax/articulo.php?op=listar_json',
      type: "GET",
      dataType: 'json',

  }).done(function(datos){
    
    var options_string = '';

    // armar html del select
    datos.forEach(function(articulo){
      options_string += '<option value="' + articulo.idarticulo +'"> ' + articulo.nombre  +'</option>';
    });

    $('#select_articulo').append(options_string);

  })
  .fail(function(){
    alert('Error al cargar artículos');
  });

}


init();