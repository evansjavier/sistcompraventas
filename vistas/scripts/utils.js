function validarDniCuit($input, modo){
	var documento_valido = false;

	switch (modo){
		case '':
		case 'DNI':
			var filtrado = $input.val().replace(/[^\d]+/g,'').substr(0, 8);
			$input.val( filtrado );			
			documento_valido = (filtrado.length == 7 || filtrado.length == 8);
		break;

		case 'CUIT':
		case 'CUIL':
			var filtrado = $input.val().replace(/[^\d]+/g,'').substr(0, 11);
			$input.val( filtrado );
			documento_valido = validaCuit(filtrado);
			break;
		default:
			console.error("modo desconocido");
		break;

	}

	return documento_valido;
}

function validaCuit(cuit) {
	if (typeof (cuit) == 'undefined')
		return true;

	cuit = cuit.toString().replace(/[-_]/g, "");

	if (cuit == '')
		return true; //No estamos validando si el campo esta vacio, eso queda para el "required"
	
	if (cuit.length != 11)
		return false;
	else {
		var mult = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
		var total = 0;
		for (var i = 0; i < mult.length; i++) {
			total += parseInt(cuit[i]) * mult[i];
		}
		var mod = total % 11;
		var digito = mod == 0 ? 0 : mod == 1 ? 9 : 11 - mod;
	}

	return digito == parseInt(cuit[10]);
}