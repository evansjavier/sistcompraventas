<?php 
ob_start();
if (strlen(session_id()) < 1){
	session_start();//Validamos si existe o no la sesión
}
if (!isset($_SESSION["nombre"]))
{
  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.
}
else
{
//Validamos el acceso solo al usuario logueado y autorizado.
if ($_SESSION['consultac']==1 || $_SESSION['consultav']==1)
{
require_once "../modelos/Consultas.php";

$consulta=new Consultas();

// Declaramos array con nombres de los meses a mostrar en reportes
$nombres_meses = [
	'Ene',
	'Feb',
	'Mar',
	'Abr',
	'May',
	'Jun',
	'Jul',
	'Ago',
	'Sep',
	'Oct',
	'Nov',
	'Dic'
];

switch ($_GET["op"]){
	case 'comprasfecha':
		$fecha_inicio=$_REQUEST["fecha_inicio"];
		$fecha_fin=$_REQUEST["fecha_fin"];

		$rspta=$consulta->comprasfecha($fecha_inicio,$fecha_fin);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>$reg->fecha,
 				"1"=>$reg->usuario,
 				"2"=>$reg->proveedor,
 				"3"=>$reg->tipo_comprobante,
 				"4"=>$reg->serie_comprobante.' - '.$reg->num_comprobante.' - '.$reg->num_comprobante_b,
 				"5"=>$reg->total_compra,
 				//"6"=>$reg->impuesto,
 				"6"=>($reg->estado=='Aceptado')?'<span class="label bg-green">Aceptado</span>':
 				'<span class="label bg-red">Anulado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;


	case 'ventasfechacliente':
		$fecha_inicio=$_REQUEST["fecha_inicio"];
		$fecha_fin=$_REQUEST["fecha_fin"];
		//$idcliente=$_REQUEST["idcliente"];

        $rspta=$consulta->ventasfechacliente($fecha_inicio,$fecha_fin/*,$idcliente*/);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>$reg->fecha,
 				"1"=>$reg->usuario,
 				"2"=>$reg->cliente,
 				"3"=>$reg->tipo_comprobante,
 				"4"=>str_pad($reg->num_comprobante, 4, "0", STR_PAD_LEFT).' - '.str_pad($reg->num_comprobante_b, 8, "0", STR_PAD_LEFT),
 				"5"=>$reg->total_venta,
 				//"6"=>$reg->impuesto,
 				"6"=>($reg->estado=='Aceptado')?'<span class="label bg-green">Aceptado</span>':
 				'<span class="label bg-red">Anulado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case 'ventas_periodo_mensual':
		$fecha_inicio=$_REQUEST["fecha_inicio"];
		$fecha_fin=$_REQUEST["fecha_fin"];

		$rspta=$consulta->ventas_periodo_mensual($fecha_inicio,$fecha_fin);

 		//Vamos a declarar un array para guardar las ventas de cada mes
		$meses= Array();
		$datos= Array();

 		while ($reg = $rspta->fetch_object()){
			$meses[] = $nombres_meses[$reg->fecha - 1]; // cambiar número de mes por nombre
			$datos[] = (float) $reg->total; // convertir total a float y agregar al array
 		}
 		echo json_encode([
			'meses' => $meses,
			'datos' => $datos
		]);
	break;


	case 'ventas_articulos_general':
		$fecha_inicio = $_REQUEST["fecha_inicio"];
		$fecha_fin = $_REQUEST["fecha_fin"];

		$rspta = $consulta->ventas_articulos_general($fecha_inicio, $fecha_fin);

 		//Vamos a declarar un array para guardar las ventas de cada articulo
		$articulos = Array();
		$datos_cantidades = Array();		

 		while ($reg = $rspta->fetch_object()){
			$articulos[] = $reg->nombre;
			$datos_cantidades[] = (float) $reg->total_cantidad;
 		}
 		echo json_encode([
			'articulos' => $articulos,
			'datos_cantidades' => $datos_cantidades,
		]);
	break;

	case 'ventas_articulo_cantidad':
		$fecha_inicio = $_REQUEST["fecha_inicio"];
		$fecha_fin = $_REQUEST["fecha_fin"];
		$articuloid = $_REQUEST["articuloid"];

		$rspta = $consulta->ventas_articulo_cantidad($fecha_inicio, $fecha_fin, $articuloid);

 		//Vamos a declarar un array para guardar las ventas de cada mes
		$meses = Array();
		$datos_cantidades = Array();
		$datos_ventas = Array();

 		while ($reg = $rspta->fetch_object()){
			$meses[] = $nombres_meses[$reg->fecha - 1]; // cambiar número de mes por nombre
			$datos_cantidades[] = (int) $reg->total_cantidad; // convertir cantidad a int y agregar al array
			$datos_ventas[] = (float) $reg->total_venta;  // convertir total a float y agregar al array
 		}
 		echo json_encode([
			'meses' => $meses,
			'datos_cantidades' => $datos_cantidades,
			'datos_ventas' => $datos_ventas
		]);
	break;
}
//Fin de las validaciones de acceso
}
else
{
  require 'noacceso.php';
}
}
ob_end_flush();
?>